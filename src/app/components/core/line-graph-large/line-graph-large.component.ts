import { Component, OnInit, Input, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import {Chart, ChartDataSets, ChartData} from 'chart.js';
import { element } from '@angular/core/src/render3/instructions';

@Component({
  selector: 'app-line-graph-large',
  templateUrl: './line-graph-large.component.html',
  styleUrls: ['./line-graph-large.component.scss']
})
export class LineGraphLargeComponent implements OnInit, AfterViewInit {

  // @Input() graphData: Array<Object>;
  @ViewChild('canvas') canvas: ElementRef;
  @Input() data: ChartDataSets [];
  @Input() labels: string[];
  dataPayload: any;
  graph_data = [];
  chart = [];

  ngOnInit() {

  }

  ngAfterViewInit() {
    this.graph_data.push(new Chart(this.canvas.nativeElement.getContext('2d'), {
      type: 'line',
      data: {
        labels: this.labels,
        datasets: this.data
      },

      options: {
        responsive: true,
        legend: { display: true},
        scales: {
          yAxes: [{
            ticks: {
              display: true,
              beginAtZero: false,
              maxTicksLimit: 6
            },
            gridLines: {
              display: true,
              drawBorder: false
            }
          }],
          xAxes: [{
            ticks: {
              display: true,
              beginAtZero: true
            },
            gridLines: {
              display: false
            }
          }]
        }
      }
    }))

  }

}
