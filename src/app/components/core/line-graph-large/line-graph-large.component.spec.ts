import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LineGraphLargeComponent } from './line-graph-large.component';

describe('LineGraphLargeComponent', () => {
  let component: LineGraphLargeComponent;
  let fixture: ComponentFixture<LineGraphLargeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LineGraphLargeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LineGraphLargeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
