import { Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartData } from 'chart.js';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {

  public mpesa_data_array= [28, 48, 40, 19, 86, 27, 90, 40, 19, 86, 27, 90];
  public card_data_array = [65, 59, 80, 81, 56, 55, 59, 81, 80, 80, 56, 67];
  public graph_data: ChartDataSets [] = [
    {
      data: this.card_data_array,
      label: 'Card / VISA',
      borderJoinStyle: 'bevel',
      pointBackgroundColor: 'rgb(63, 87, 111, .8)',
      pointHoverBorderColor: 'rgb(63, 87, 111, .8)',
      pointHoverBorderWidth: 2,
      backgroundColor: 'rgb(63, 87, 111, .3)',
      borderColor: 'rgb(63, 87, 111,.6)'},
    {
      data: this.mpesa_data_array,
      label: 'M-PESA',
      pointBackgroundColor: 'rgb(65, 200, 80, .8)',
      pointHoverBorderColor: 'rgb(65, 200, 80, .8)',
      pointHoverBorderWidth: 2,
      backgroundColor: 'rgb(65, 200, 80, .3)',
      borderColor: 'rgb(65, 200, 80,.6)'}
  ];

  public graph_labels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  constructor() { }

  ngOnInit() {
  }
}
