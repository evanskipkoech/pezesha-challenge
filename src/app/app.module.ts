import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RoutingModule} from './modules/routing/routing.module';
import { ChartsModule} from 'ng2-charts';
import { OwlModule } from 'ngx-owl-carousel';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/core/navbar/navbar.component';
import { HomeComponent } from './pages/home/home.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { RegisterComponent } from './pages/register/register.component';
import { AboutUsComponent } from './pages/about-us/about-us.component';
import { FooterComponent } from './components/core/footer/footer.component';
import { LineGraphLargeComponent } from './components/core/line-graph-large/line-graph-large.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    PageNotFoundComponent,
    RegisterComponent,
    AboutUsComponent,
    FooterComponent,
    LineGraphLargeComponent
  ],
  imports: [
    BrowserModule,
    ChartsModule,
    RoutingModule,
    HttpClientModule,
    OwlModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
