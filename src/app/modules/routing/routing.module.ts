import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes } from '@angular/router';

import {HomeComponent} from '../../pages/home/home.component';
import {PageNotFoundComponent} from '../../pages/page-not-found/page-not-found.component';
import {RegisterComponent} from '../../pages/register/register.component';
import {AboutUsComponent} from '../../pages/about-us/about-us.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent },
  { path: 'about-us', component: AboutUsComponent},
  { path: 'register', component: RegisterComponent},
  { path: '404', component: PageNotFoundComponent},
  { path: '**', redirectTo: '404'},
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(
      appRoutes, { onSameUrlNavigation: 'reload' }
    ),
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class RoutingModule { }
